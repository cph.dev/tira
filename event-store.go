package tira

import (
	"context"
	"github.com/bruth/rita"
)

type EventStore[T rita.Evolver] interface {
	// Append event to stream
	//
	// expectSequence must be correct in order for the event to be appended.
	//If this event is expected to be the first for given entity then set expectSequence to zero.
	Append(
		ctx context.Context,
		id EntityID,
		data any,
		expectSequence uint64,
	) (uint64, error)

	// Load and evolve entity into given target.
	Load(
		ctx context.Context,
		id EntityID,
		target T,
	) (uint64, error)
}
