package tira

import (
	"context"
	"errors"
	"fmt"
	"github.com/bruth/rita"
	"github.com/bruth/rita/types"
	"github.com/nats-io/nats.go"
)

func NewEventStore[T rita.Evolver](nc *nats.Conn, registry *types.Registry, streamName StreamName) (
	EventStore[T],
	error,
) {
	if nc == nil {
		return nil, errors.New("nc must not be nil")
	}
	if registry == nil {
		return nil, errors.New("registry must not be nil")
	}
	if len(streamName) == 0 {
		return nil, errors.New("stream name must not be empty")
	}
	rt, err := rita.New(nc, rita.TypeRegistry(registry))
	if err != nil {
		return nil, fmt.Errorf("unable to create new Rita toolkit. %w", err)
	}
	es := rt.EventStore(string(streamName))
	res := tiraEventStore[T]{
		rt:         rt,
		es:         es,
		streamName: streamName,
	}
	// TODO: Should StreamConfig be more configured?
	if err := es.Create(&nats.StreamConfig{}); err != nil {
		return nil, fmt.Errorf("unable to configure event store. %w", err)
	}
	return &res, nil
}

// Events are appended to ORDERS event stream.
type tiraEventStore[T rita.Evolver] struct {
	streamName StreamName
	rt         *rita.Rita
	es         *rita.EventStore
}

// Append implements EventStore interface.
//
// It returns the resulting sequence number of the last appended event.
func (res *tiraEventStore[T]) Append(
	ctx context.Context,
	id EntityID,
	data any,
	expectSequence uint64,
) (uint64, error) {
	if ctx == nil {
		return 0, errors.New("context must not be nil")
	}
	if data == nil {
		return 0, errors.New("data must not be nil")
	}
	subject := fmt.Sprintf("%s.%s", res.streamName, id)
	return res.es.Append(
		ctx,
		subject,
		[]*rita.Event{
			{
				Data: data,
				Meta: map[string]string{
					EntityIDMetaKey: string(id),
				},
			},
		},
		rita.ExpectSequence(expectSequence),
	)
}

func (res *tiraEventStore[T]) Load(
	ctx context.Context,
	id EntityID,
	target T,
) (uint64, error) {
	if ctx == nil {
		return 0, errors.New("context must not be nil")
	}
	if len(id) == 0 {
		return 0, errors.New("id must not be empty")
	}
	subject := fmt.Sprintf("%s.%s", res.streamName, id)
	lastSeq, err := res.es.Evolve(ctx, subject, target)
	if err != nil {
		return 0, fmt.Errorf("unable to evolve %q ti %T: %w", subject, target, err)
	}
	return lastSeq, nil
}
