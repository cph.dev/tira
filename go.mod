module go.cph.dev/tira

go 1.20

require (
	github.com/bruth/rita v0.0.0-20230622010146-1c52b4b6c9cd // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/nats-io/nats.go v1.15.0 // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220315160706-3147a52a75dd // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
