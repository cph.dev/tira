package tira

import "fmt"

// Entity represents any struct or type that are identified by an EntityID.
type Entity interface {
	EntityID() EntityID
}

type EntityID string

type StreamName string

func (name StreamName) Subject() string {
	return fmt.Sprintf("%s.*", name)
}

const EntityIDMetaKey = "EntityID"
