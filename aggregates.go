package tira

import "github.com/bruth/rita"

// Aggregate defines core behaviour of an aggregate.
//
// Can evolve based on events and has identity.
type Aggregate interface {

	// Evolver means aggregates can be evolved with events
	rita.Evolver

	// Entity means aggregates have identity
	Entity
}
